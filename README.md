# Cadastro de contatos

Projeto do curso de Engenharia de Software da 1ª fase da CatolicaSC de Jaragua do Sul. Desenvolvimento de uma agenda de contatos com CRUD basico com persistencia utilizando ANSI C.

### Executar

Esse programa foi testado no MacOS e Windows 8. Para compilar basta executar o comando abaixo

```
$ gcc contacts.c -o bin/contacts.out
$ ./bin/contacts.out
```
